﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ModemControl;

namespace OptimaTablesTest
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<ModemControl.ModemModel> listModem = new List<ModemControl.ModemModel>();
        
        public MainWindow()
        {
            InitializeComponent();

            DataContext = this;

            tabl.SetTranslation(ModemControl.Language.RU);

            tabl.ListModemModel = new List<ModemModel>()
            {
                new ModemModel(){Ethernet = SignalLevel.NoSignal, Radio = SignalLevel.Low, IPAddress = "127.0.0.0", GLONASS = new JammingSystem(){L1 = Led.Green, L2 = Led.Red},
                TimeTask = "1111111111111111111111111" +
                "\n34343434343434343434343434" +
                "\n343434343434343434343434343434455"+
                "\n435656245245566511111111111111"}
            };

            

            //tabl.VisibilityRadioColumn = Visibility.Collapsed;
        }

        private void ucModem_OnAddRecord(object sender, ModemControl.TableEvents e)
        {
            listModem.Add(e.Record);
           // ucModem.ListModemModel = listModem;
        }

        private void ucModem_OnChangeRecord(object sender, ModemControl.TableEvents e)
        {
            int ind = listModem.FindIndex(x => x.Number == e.Record.Number);
            if (ind != -1)
            {
                listModem[ind] = e.Record;
            }
           // ucModem.ListModemModel = listModem;
        }

        private void ucModem_OnDeleteRecord(object sender, ModemControl.TableEvents e)
        {
            int ind = listModem.FindIndex(x => x.Number == e.Record.Number);
            if (ind != -1)
            {
                listModem.RemoveAt(ind);
            }
           // ucModem.ListModemModel = listModem;
        }

        private void ucModem_OnClearRecords(object sender, EventArgs e)
        {
            listModem = new List<ModemControl.ModemModel>();
           // ucModem.ListModemModel = listModem;
        }
     

        private void ucModem_OnAddRecord(object sender, RoutedEventArgs e)
        {

        }
    }
}
