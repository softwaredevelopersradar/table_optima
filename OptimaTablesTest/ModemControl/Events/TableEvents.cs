﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModemControl
{
    public class TableEvents : EventArgs
    {
        public ModemModel Record { get; }

        public TableEvents(ModemModel rec)
        {
            Record = rec;
        }
    }
}
