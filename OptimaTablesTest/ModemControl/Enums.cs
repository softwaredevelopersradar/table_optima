﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModemControl
{
    public enum Led : byte
    {
        Empty = 0,
        Green = 1,
        Red = 2,
        Gray = 3,
        Yellow = 4
    }

    public enum SignalLevel : byte
    {
        Empty = 0,
        NoSignal = 1,
        VeryLow = 2,
        Low = 3,
        Medium = 4,
        Hight = 5,
    }

    public enum Language : byte
    {
        RU = 0,
        EN = 1,
        AZ = 2
    }

    public enum ViewCoord : byte
    {
        [Description("DD.dddddd")] Dd = 1,
        [Description("DD MM.mmmm")] DMm = 2,
        [Description("DD MM SS.ss")] DMSs = 3,
    }
}
