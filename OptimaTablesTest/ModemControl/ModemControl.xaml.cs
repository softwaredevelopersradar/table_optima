﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ModemControl.Model;
using Xceed.Wpf.Toolkit.Core.Converters;
using YamlDotNet.Core.Tokens;


namespace ModemControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlModem : UserControl, INotifyPropertyChanged
    {
        #region Events

        public event EventHandler<ModemModel> OnSelectionChanged = (sender, obj) => { };
        #endregion

        #region Properties

        private readonly VisibilityConnectionTypesModel _visibilityConnectionTypes = new VisibilityConnectionTypesModel();

        public static readonly DependencyProperty DependencyViewCoord =
            DependencyProperty.Register("ViewCoord", typeof(ViewCoord), typeof(UserControlModem), new UIPropertyMetadata(OnDependencyPropertyChanged));

        public static readonly DependencyProperty DependencyLanguage =
            DependencyProperty.Register("Language", typeof(Language), typeof(UserControlModem), new UIPropertyMetadata(OnDependencyPropertyChanged));

        private static readonly DependencyProperty dependencyVisibilityRaqdioFColumn =
            DependencyProperty.Register("VisibilityRadioFColumn", typeof(Visibility), typeof(UserControlModem), new UIPropertyMetadata(OnDependencyPropertyChanged));

        private static readonly DependencyProperty dependencyVisibilityRaqdioColumn =
            DependencyProperty.Register("VisibilityRadioColumn", typeof(Visibility), typeof(UserControlModem), new UIPropertyMetadata(OnDependencyPropertyChanged));

        private static readonly DependencyProperty dependencyVisibilityGSMColumn =
            DependencyProperty.Register("VisibilityGSMColumn", typeof(Visibility), typeof(UserControlModem), new UIPropertyMetadata(OnDependencyPropertyChanged));

        private static readonly DependencyProperty dependencyVisibilityStateColumn =
            DependencyProperty.Register("VisibilityStateColumn", typeof(Visibility), typeof(UserControlModem), new UIPropertyMetadata(OnDependencyPropertyChanged));

        private static readonly DependencyProperty dependencyVisibilityIPAddressColumn =
            DependencyProperty.Register("VisibilityIPAddressColumn", typeof(Visibility), typeof(UserControlModem), new UIPropertyMetadata(OnDependencyPropertyChanged));

        private static readonly DependencyProperty dependencyVisibilityCoordinatesColumn =
            DependencyProperty.Register("VisibilityCoordinatesColumn", typeof(Visibility), typeof(UserControlModem), new UIPropertyMetadata(OnDependencyPropertyChanged));

        private static readonly DependencyProperty dependencyVisibilityRMColumn =
            DependencyProperty.Register("VisibilityRMColumn", typeof(Visibility), typeof(UserControlModem), new UIPropertyMetadata(OnDependencyPropertyChanged));

        private static readonly DependencyProperty dependencyVisibilityEthernetColumn =
            DependencyProperty.Register("VisibilityEthernetColumn", typeof(Visibility), typeof(UserControlModem), new UIPropertyMetadata(OnDependencyPropertyChanged));


        private static void OnDependencyPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            UserControlModem userControlModem = d as UserControlModem;
            userControlModem.OnDependencyPropertyChanged(e);
        }

        private void OnDependencyPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
         
            switch (e.Property.Name)
            {
                case nameof(VisibilityRadioFColumn):
                    RadioFColumn.Visibility = (Visibility)e.NewValue;
                    break;
                case nameof(VisibilityRadioColumn):
                    _visibilityConnectionTypes.VisibilityOfRadioColumn = (Visibility)e.NewValue;
                    ChangeVsibilityInConnectiontype();
                    break;
                case nameof(VisibilityGSMColumn):
                    _visibilityConnectionTypes.VisibilityOfGSMColumn = (Visibility)e.NewValue;
                    ChangeVsibilityInConnectiontype();
                    break;
                case nameof(VisibilityStateColumn):
                    StateColumn.Visibility = (Visibility)e.NewValue;
                    break;
                case nameof(VisibilityIPAddressColumn):
                    IPAddressColumn.Visibility = (Visibility)e.NewValue;
                    break;
                case nameof(VisibilityCoordinatesColumn):
                    CoordinatesColumn.Visibility = (Visibility)e.NewValue;
                    break;
                case nameof(VisibilityRMColumn):
                    RMColumn.Visibility = (Visibility)e.NewValue;
                    break;
                case nameof(VisibilityEthernetColumn):
                    _visibilityConnectionTypes.VisibilityOfEthernetColumn = (Visibility)e.NewValue;
                    ChangeVsibilityInConnectiontype();
                    break;
                case nameof(Language):
                    SetTranslation((Language)e.NewValue);
                    break;
                case nameof(ViewCoord):
                    foreach (var model in listModemModel)
                        model.ViewCoordField = (ViewCoord)e.NewValue;
                    break;
            }
        }

        private void ChangeVsibilityInConnectiontype()
        {
            GSMLabel.Visibility = _visibilityConnectionTypes.VisibilityOfGSMColumn;
            RadioLabel.Visibility = _visibilityConnectionTypes.VisibilityOfRadioColumn;
            EthernetLabel.Visibility = _visibilityConnectionTypes.VisibilityOfEthernetColumn;
            LeftBorder.Visibility = _visibilityConnectionTypes.VisibilityLeftBorder;
            RightBorder.Visibility = _visibilityConnectionTypes.VisibilityRightBorder;
            ConnectionTypesColumn.MaxWidth = ConnectionTypesColumn.MinWidth = ConnectionTypesGrid.Width = _visibilityConnectionTypes.WidthOfColumn;
            //Grid.SetColumnSpan(ConnectionTypeLable, _visibilityConnectionTypes.ColumnSpanOfTopText);

        }

        public new ViewCoord ViewCoord
        {
            get => (ViewCoord)GetValue(DependencyViewCoord);
            set => SetValue(DependencyViewCoord, value);
        }

        public new Language Language
        {
            get => (Language)GetValue(DependencyLanguage);
            set => SetValue(DependencyLanguage, value);
        }


        public Visibility VisibilityRadioFColumn
        {
            get => (Visibility)GetValue(dependencyVisibilityRaqdioFColumn);
            set => SetValue(dependencyVisibilityRaqdioFColumn, value);
        }

        public Visibility VisibilityRadioColumn
        {
            get => (Visibility)GetValue(dependencyVisibilityRaqdioColumn);
            set => SetValue(dependencyVisibilityRaqdioColumn, value);
        }

        public Visibility VisibilityGSMColumn
        {
            get => (Visibility)GetValue(dependencyVisibilityGSMColumn);
            set => SetValue(dependencyVisibilityGSMColumn, value);
        }

        public Visibility VisibilityStateColumn
        {
            get => (Visibility)GetValue(dependencyVisibilityStateColumn);
            set => SetValue(dependencyVisibilityStateColumn, value);
        }

        public Visibility VisibilityIPAddressColumn
        {
            get => (Visibility)GetValue(dependencyVisibilityIPAddressColumn);
            set => SetValue(dependencyVisibilityIPAddressColumn, value);
        }

        public Visibility VisibilityCoordinatesColumn
        {
            get => (Visibility)GetValue(dependencyVisibilityCoordinatesColumn);
            set => SetValue(dependencyVisibilityCoordinatesColumn, value);
        }

        public Visibility VisibilityRMColumn
        {
            get => (Visibility)GetValue(dependencyVisibilityRMColumn);
            set => SetValue(dependencyVisibilityRMColumn, value);
        }

        public Visibility VisibilityEthernetColumn
        {
            get => (Visibility)GetValue(dependencyVisibilityEthernetColumn);
            set => SetValue(dependencyVisibilityEthernetColumn, value);
        }

     

        public int AmountOfNeseseryRows { get; set; }

        #endregion

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion


     

        public UserControlModem()
        {
            InitializeComponent();

            DgvModem.DataContext = new GlobalModem();

            DgvModem.MouseDoubleClick += DgvModem_MouseDoubleClick;
        }

        private void DgvModem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            OnSelectionChanged(sender, ((ModemModel)DgvModem.SelectedItem));
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((ModemModel)DgvModem.SelectedItem).Number > 0)
                {
                    if (((ModemModel)DgvModem.SelectedItem).IsActive)
                    {
                        ((ModemModel)DgvModem.SelectedItem).IsActive = false;
                    }
                    else
                    {
                        ((ModemModel)DgvModem.SelectedItem).IsActive = true;
                    }

                    // Событие изменения одной записи
                    // OnChangeRecord(this, new TableEvents((ModemModel)DgvModem.SelectedItem));
                }
                else
                {
                    CheckBox chbIsChecked = sender as CheckBox;
                    chbIsChecked.IsChecked = false;
                }
            }
            catch { }
        }

        private void DgvModem_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //if(DgvModem.Items.Count < AmountOfNeseseryRows) // Иначе оно циклится и начинает добавлять кучу строк
                AddEmptyRows();
        }


        public void ChangeSelectedIndex(int i)
        {
            PropertyModem.SelectedNumber = i;
        }

        private void DgvModem_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {        

            if ((ModemModel)DgvModem.SelectedItem == null) return;

            if (((ModemModel)DgvModem.SelectedItem).Number > 0)
                PropertyModem.SelectedNumber = ((ModemModel)DgvModem.SelectedItem).Number;        
        }

        private void TBCheckedAll_OnClick(object sender, RoutedEventArgs e)
        {
            if (chbCheckAll.IsChecked.Value)
            {
                foreach (var item in ListModemModel)
                {
                    item.IsActive = true;
                }
            }
            else
            {
                foreach (var item in ListModemModel)
                {
                    item.IsActive = false;
                }
            }

            UpdateModem();
        }
    }
}
