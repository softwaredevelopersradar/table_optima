﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace ModemControl
{
    public class EmptyStringToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (((string)value).Equals("") || ((string)value).Equals(" ") || !((string)value).Contains("\n"))
                return ScrollBarVisibility.Disabled;
            return ScrollBarVisibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
