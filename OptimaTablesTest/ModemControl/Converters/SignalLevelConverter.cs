﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ModemControl
{
    [ValueConversion(sourceType: typeof(SignalLevel), targetType: typeof(Uri))]
    public class SignalLevelConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            Uri uri;
            switch (value)
            {
                case SignalLevel.Empty:
                    uri = new Uri(@"pack://application:,,,/"
                               + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                               + ";component/"
                               + "Resources/empty_signal_level.png", UriKind.Absolute);
                    break;

                case SignalLevel.NoSignal:
                    uri = new Uri(@"pack://application:,,,/"
                              + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                              + ";component/"
                              + "Resources/NoSignal.png", UriKind.Absolute);
                    break;

                case SignalLevel.VeryLow:
                    uri = new Uri(@"pack://application:,,,/"
                              + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                              + ";component/"
                              + "Resources/VeryLow.png", UriKind.Absolute);
                    break;

                case SignalLevel.Low:
                    uri = new Uri(@"pack://application:,,,/"
                              + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                              + ";component/"
                              + "Resources/Low.png", UriKind.Absolute);
                    break;

                case SignalLevel.Medium:
                    uri = new Uri(@"pack://application:,,,/"
                              + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                              + ";component/"
                              + "Resources/Medium.png", UriKind.Absolute);
                    break;

                case SignalLevel.Hight:
                    uri = new Uri(@"pack://application:,,,/"
                              + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                              + ";component/"
                              + "Resources/Heigh.png", UriKind.Absolute);
                    break;
                default:
                    uri = new Uri(@"pack://application:,,,/"
                              + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                              + ";component/"
                              + "Resources/NoSignal.png", UriKind.Absolute);
                    break;
            }

            return uri;
            //}
            //catch { return null; }
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
