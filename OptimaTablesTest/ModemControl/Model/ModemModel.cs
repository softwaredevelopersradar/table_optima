﻿using ModemControl.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Markup.Localizer;
using System.Windows.Media.Animation;
using System.Xml.Serialization;
using YamlDotNet.Serialization;

namespace ModemControl 
{
    [DataContract]
    [CategoryOrder("Common", 1)]
    [CategoryOrder("Coordinates", 2)]
    [CategoryOrder("Connections", 3)]
    public class ModemModel : AbstractModelWithCoordTranslation
    {
        public ModemModel()
        {
            GPS = new JammingSystem();
            GLONASS = new JammingSystem();
            Galileo = new JammingSystem();
            Beidou = new JammingSystem();
            RadioF = new Radio();
            RM = Led.Gray;
        }

        [YamlIgnore]
        [Browsable(false)]
        public int Id { get; set; }

        [YamlIgnore]
        [Browsable(false)]
        public bool IsActive { get; set; }

        [YamlIgnore]
        [Browsable(false)]
        public JammingSystem GPS { get; set; }

        [YamlIgnore]
        [Browsable(false)]
        public JammingSystem GLONASS { get; set; }

        [YamlIgnore]
        [Browsable(false)]
        public JammingSystem Galileo{ get; set; }

        [YamlIgnore]
        [Browsable(false)]
        public JammingSystem Beidou { get; set; }

        [YamlIgnore]
        [Browsable(false)]
        public Radio RadioF { get; set; }

        [YamlIgnore]
        [Browsable(false)] 
        public SignalLevel GSM { get; set; } = SignalLevel.NoSignal;

        [YamlIgnore]
        [Browsable(false)]
        public SignalLevel Radio { get; set; } = SignalLevel.NoSignal;

        [YamlIgnore]
        [Browsable(false)]
        public SignalLevel Ethernet { get; set; } = SignalLevel.NoSignal;

        [YamlIgnore]
        [Browsable(false)]
        public Led RM { get; set; }

        [YamlIgnore]
        [Browsable(false)] 
        public string State { get; set; } = "Disconnected";

        private string _ipAddress = "37.17.12.133";
        private byte _number = 0;
        private string _note = "";
        private string _timeTask = "";

        public const string CommonCategory = "Common";
        public const string ConnectionsCategory = "Connections";


        [NotifyParentProperty(true)]
        [Category(CommonCategory)]
        [DisplayName(nameof(Number))]
        [PropertyOrder(0)]
        public byte Number
        {
            get => _number;
            set
            {
                if (_number == value) return;
                _number = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        [Category(CommonCategory)]
        [DisplayName(nameof(Note))]
        [PropertyOrder(1)]
        public string Note
        {
            get => _note;
            set
            {
                if (_note == value) return;
                _note = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(CommonCategory)]
        [DisplayName(nameof(TimeTask))]
        [PropertyOrder(1)]
        public string TimeTask
        {
            get => _timeTask;
            set
            {
                if (_timeTask == value) return;
                _timeTask = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        [Category(ConnectionsCategory)]
        [DisplayName(nameof(IPAddress))]
        [Required]
        [PropertyOrder(0)]
        [RegularExpression(@"^(?!127\.0\.0\.1)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))$", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IPAddress
        {
            get => _ipAddress;
            set
            {
                if (_ipAddress == value) return;
                _ipAddress = value;
                OnPropertyChanged();
            }
        }


        public override string ToString()
        {
            string num = "";
            switch (Number - 1)
            {
                case 0: num = "98"; break;
                case 1: num = "99"; break;
                case 2: num = "0"; break;
                case 3: num = "1"; break;
                case 4: num = "2"; break;
                case 5: num = "3"; break;
                case 6: num = "4"; break;
                case 7: num = "5"; break;
                case 8: num = "6"; break;
                case 9: num = "7"; break;
                default: num = "-1"; break;
            }

            return $"{num} : {IsLaunchSystem(GPS,nameof(GPS))}; {IsLaunchSystem(GLONASS,nameof(GLONASS))}; {IsLaunchSystem(Galileo,nameof(Galileo))}; {IsLaunchSystem(Beidou,nameof(Beidou))}";
        }

        private string IsLaunchSystem(JammingSystem jammingSystem, string header)
        {
            return $"{header}: {IsLaunchL(jammingSystem.L1, header)}, {IsLaunchL(jammingSystem.L2, header)}";
        }

        private string IsLaunchL(Led L, string header)
        {
            switch (L)
            {
                case Led.Green:
                    return $"{header} - On";
                case Led.Red:
                    return $"{header} - Off";
                case Led.Yellow:
                    return $"{header} - Without radiation";
                default:
                    return "";
            }
        }

        public ModemModel Clone()
        {
            return new ModemModel
            {
                Id = Id,
                IsActive = IsActive,
                Number = Number,
                GSM = GSM,
                Radio = Radio,
                Galileo = Galileo,
                Beidou = Beidou,
                GLONASS = GLONASS,
                GPS = GPS,
                RadioF = RadioF,
                RM = RM,                
                IPAddress = IPAddress,
                LatitudeStr = LatitudeStr,
                LongitudeStr = LongitudeStr,
                Latitude = Latitude,
                Longitude = Longitude,
                Note = Note,
                State = State,
                Ethernet = Ethernet,
                TimeTask = TimeTask
            };
        }

        public bool EqualTo(ModemModel model)
        {
            return Id == model.Id 
                && IsActive == model.IsActive
                && Number == model.Number
                && GSM == model.GSM
                && Radio == model.Radio
                && Galileo == model.Galileo
                && Beidou == model.Beidou
                && GLONASS == model.GLONASS
                && GPS == model.GPS
                && RadioF == model.RadioF
                && RM == model.RM
                && IPAddress == model.IPAddress
                && LatitudeStr == model.LatitudeStr
                && LongitudeStr == model.LongitudeStr
                && Latitude == model.Latitude
                && Longitude == model.Longitude
                && Note == model.Note
                && State == model.State
                && Ethernet == model.Ethernet
                && TimeTask == model.TimeTask;          
        }

        public void Update(ModemModel model)
        {
            Id = model.Id;
            IsActive = model.IsActive;
            Number = model.Number;
            GSM = model.GSM;
            Radio = model.Radio;
            Galileo = model.Galileo;
            Beidou = model.Beidou;
            GLONASS = model.GLONASS;
            GPS = model.GPS;
            RadioF = model.RadioF;
            RM = model.RM;   
            IPAddress = model.IPAddress;
            LatitudeStr = model.LatitudeStr;
            LongitudeStr = model.LongitudeStr;
            Latitude = model.Latitude;
            Longitude = model.Longitude;
            Note = model.Note;
            State = model.State;
            Ethernet = model.Ethernet;
            TimeTask = model.TimeTask;
        }     
    }

    public class JammingSystem : AbstractDataErrorInfo
    {
        public Led L1 { get; set; } = Led.Gray;
        public Led L2 { get; set; } = Led.Gray;
        public Led L3 { get; set; } = Led.Gray;
    }
    public class Radio : AbstractDataErrorInfo
    {
        public Led F1 { get; set; } = Led.Gray;
        public Led F2 { get; set; } = Led.Gray;
    }
}
