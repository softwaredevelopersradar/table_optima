﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ModemControl.Model
{
    public class VisibilityConnectionTypesModel : INotifyPropertyChanged
    {
        private Visibility visibilityOfRadioColumn = Visibility.Visible;
        private Visibility visibilityOfGSMColumn = Visibility.Visible;
        private Visibility visibilityOfEthernetColumn = Visibility.Visible;
        private Visibility leftBorder = Visibility.Visible;
        private Visibility rightBorder = Visibility.Visible;
        private int widthOfColumn;
        private int columnSpanOfTopText;

        public Visibility VisibilityOfRadioColumn
        {
            get => visibilityOfRadioColumn;
            set
            {
                if(visibilityOfRadioColumn == value) return;
                visibilityOfRadioColumn = value;
                ColumnVisibility();
                OnPropertyChanged();
            }
        }

        public Visibility VisibilityOfGSMColumn
        {
            get => visibilityOfGSMColumn;
            set
            {
                if (visibilityOfGSMColumn == value) return;
                visibilityOfGSMColumn = value;
                ColumnVisibility();
                OnPropertyChanged();
            }
        }

        public Visibility VisibilityOfEthernetColumn
        {
            get => visibilityOfEthernetColumn;
            set
            {
                if (visibilityOfEthernetColumn == value) return;
                visibilityOfEthernetColumn = value;
                ColumnVisibility();
                OnPropertyChanged();
            }
        }

        public Visibility VisibilityLeftBorder
        {
            get => leftBorder;
            set
            {
                if(leftBorder == value) return; 
                leftBorder = value;
                OnPropertyChanged();
            }
        }

        public Visibility VisibilityRightBorder
        {
            get => rightBorder;
            set
            {
                if (rightBorder == value) return;
                leftBorder = value;
                OnPropertyChanged();
            }
        }

        public int WidthOfColumn
        {
            get => widthOfColumn;
            set
            {
                if(widthOfColumn == value) return;
                widthOfColumn = value;
                OnPropertyChanged();
            }
        }
        public int ColumnSpanOfTopText
        {
            get => columnSpanOfTopText;
            set
            {
                if(columnSpanOfTopText == value) return;
                columnSpanOfTopText = value;
                OnPropertyChanged();
            }
        }

        private void ColumnVisibility()
        {
            bool gsm = VisibilityOfGSMColumn == Visibility.Visible;
            bool radio = VisibilityOfRadioColumn == Visibility.Visible;
            bool ethernet = VisibilityOfEthernetColumn == Visibility.Visible;


            if (gsm && ethernet && radio)
            {
                WidthOfColumn = 160;
                leftBorder = Visibility.Visible;
                rightBorder = Visibility.Visible;
            }
            else if ((gsm && !ethernet && radio ) || (gsm && ethernet && !radio))
            {
                WidthOfColumn = 107;
                leftBorder = Visibility.Visible;
                rightBorder = Visibility.Collapsed;
            }
            else if (!gsm && ethernet && radio )
            {
                WidthOfColumn = 107;
                leftBorder = Visibility.Collapsed;
                rightBorder = Visibility.Visible;
            }
            else if ((!gsm && ethernet && !radio ) || (!gsm && !ethernet && radio) || (gsm && !ethernet && !radio))
            {
                WidthOfColumn = 53;
                leftBorder = Visibility.Collapsed;
                rightBorder = Visibility.Collapsed;
            }
        }

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion
    }
}
