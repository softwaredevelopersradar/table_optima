﻿using System.Collections.Generic;

namespace ModemControl
{
    public class FunctionsTranslate
    {
        /// <summary>
        /// Переименование значений при смене языка
        /// </summary>
        /// <param name="TranslateDic"></param>
        public static void RenameMeaning(Dictionary<string, string> TranslateDic)
        {
            if (TranslateDic.ContainsKey("meaningAddRecord"))
                SMeaning.meaningAddRecord = TranslateDic["meaningAddRecord"];

            if (TranslateDic.ContainsKey("meaningChangeRecord"))
                SMeaning.meaningChangeRecord = TranslateDic["meaningChangeRecord"];
        }

        /// <summary>
        /// Переименование сообщений при смене языка
        /// </summary>
        /// <param name="Dictionary"></param>
        public static void RenameMessages(Dictionary<string, string> TranslateDic)
        {
            if (TranslateDic.ContainsKey("mesMessage"))
                SMessages.mesMessage = TranslateDic["mesMessage"];

            if (TranslateDic.ContainsKey("mesNumber"))
                SMessages.mesNumber = TranslateDic["mesNumber"];

            if (TranslateDic.ContainsKey("mesAlreadyExists"))
                SMessages.mesAlreadyExists = TranslateDic["mesAlreadyExists"];
        }
    }
}
