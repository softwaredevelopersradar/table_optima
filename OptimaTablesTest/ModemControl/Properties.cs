﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModemControl
{
    public class PropertyModem
    {
        public static byte BudNumber
        {
            get { return budNumber; }
            set
            {
                if (budNumber != value)
                    budNumber = value;
            }
        }
        private static byte budNumber = 1;

        public static int SelectedNumber
        {
            get { return selectedNumber; }
            set
            {
                if (selectedNumber != value)
                    selectedNumber = value;
            }
        }
        private static int selectedNumber = 0;

    }

}
