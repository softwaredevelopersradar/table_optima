﻿using ModemControl.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ModemControl
{
    public class GlobalModem : INotifyPropertyChanged
    {
       
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        public ObservableCollection<ModemModel> CollectionModem { get; set; }

        public GlobalModem()
        {
            CollectionModem = new ObservableCollection<ModemModel> { };

            #region Test
            //CollectionModem = new ObservableCollection<ModemModel>
            //{
            //    new ModemModel
            //    {
            //        Id = 1,
            //        IsActive = true,
            //        Number = 1,
            //        GPS = new GPS
            //        {
            //            L1 = Led.Gray,
            //            L2 = Led.Green
            //        },
            //        GLONASS = new GLONASS
            //        {
            //            L1 = Led.Green,
            //            L2 = Led.Red
            //        },
            //        RadioF = new Radio
            //        {
            //            F1 = Led.Red,
            //            F2 = Led.Red
            //        },
            //        GSM = Led.Green,
            //        Radio = Led.Red,
            //        RM = Led.Gray,
            //        IPAddress = "192.168.0.80",
            //        Latitude = -57.123456,
            //        Longitude = 28.654321,
            //        Note = "JJLjj"
            //    },
            //    new ModemModel
            //    {
            //        Id = 2,
            //        IsActive = false,
            //        Number = 2,
            //        GPS = new GPS
            //        {
            //            L1 = Led.Red,
            //            L2 = Led.Red
            //        },
            //        GLONASS = new GLONASS
            //        {
            //            L1 = Led.Red,
            //            L2 = Led.Green
            //        },
            //        RadioF = new Radio
            //        {
            //            F1 = Led.Gray,
            //            F2 = Led.Green
            //        },
            //        GSM = Led.Red,
            //        Radio = Led.Gray,
            //        RM = Led.Green,
            //        IPAddress = "255.255.255.255",
            //        Latitude = 56.665544,
            //        Longitude = -30.899809,
            //        Note = "MKJKHJ"
            //    }
            //};
            #endregion
        }
    }
}
