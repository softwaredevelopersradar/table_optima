﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Xceed.Wpf.Toolkit;

namespace ModemControl
{
    public static class ByteUpDownEditor
    {
        #region ByteUpDown Number
        public static readonly DependencyProperty ByteUpDownNumberProperty = DependencyProperty.RegisterAttached("ByteUpDownNumber", typeof(bool), typeof(ByteUpDownEditor), 
            new FrameworkPropertyMetadata(false, OnByteUpDownNumberProperty));

        private static void OnByteUpDownNumberProperty(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var byteUpDown = sender as ByteUpDown;
            if (byteUpDown == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);
            if (wasBound) 
            {
                byteUpDown.ValueChanged -= ByteUpDownNumber_ValueChanged;
                byteUpDown.Loaded -= ByteUpDown_Loaded;
            }
            if (needToBind) 
            { 
                byteUpDown.ValueChanged += ByteUpDownNumber_ValueChanged;
                byteUpDown.Loaded += ByteUpDown_Loaded;
            }
        }

        private static void ByteUpDown_Loaded(object sender, RoutedEventArgs e)
        {
            var byteUpDown = sender as ByteUpDown;
            if (byteUpDown == null) return;

            byteUpDown.Value = PropertyModem.BudNumber;

            BindingExpression expression = byteUpDown.GetBindingExpression(ByteUpDown.TextProperty);
            if (expression != null)
            {
                expression.UpdateSource();
            }
        }

        private static void ByteUpDownNumber_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var byteUpDown = sender as ByteUpDown;
            if (byteUpDown == null) return;

            BindingExpression expression = byteUpDown.GetBindingExpression(ByteUpDown.TextProperty);
            if (expression != null)
            {
                expression.UpdateSource();
            }
            e.Handled = true;
        }

        public static void SetByteUpDownNumberValue(ByteUpDown target, bool value)
        {
            target.SetValue(ByteUpDownNumberProperty, value);
        }

        public static bool GetByteUpDownNumberValue(ByteUpDown target)
        {
            return (bool)target.GetValue(ByteUpDownNumberProperty);
        }
        #endregion
    }
}
